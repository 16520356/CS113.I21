#include "Clipping.h"
#include <iostream>
using namespace std;
RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if ((c1 != 0) && (c2 != 0) && (c1&c2 != 0))
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int chk = CheckCase(c1, c2);
	while (chk == 3) {
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		chk = CheckCase(c1, c2);
	}
	if (chk == 2)
		return 0;
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	double dx = P2.x - P1.x;
	double dy = P2.y - P1.y;
	double x = P1.x;
	double y = P1.y;
	if (c1 == 0) {
		Vector2D A;
		A = P1;
		P1 = P2;
		P2 = A;
		return;
	}
	if (c1 & TOP) {
		y = r.Top;
		x = P1.x + dx / dy*(y - P1.y);
	}else
		if (c1 & BOTTOM) {
			y = r.Bottom;
			x = P1.x + dx / dy*(y - P1.y);
		}else
			if (c1 & RIGHT) {
				x = r.Right;
				y = P1.y + dy / dx*(x - P1.x);
			}else
				if (c1 & LEFT) {
					x = r.Left;
					y = P1.y + dy / dx*(x - P1.x);
				}
	P1.x = x;
	P1.y = y;
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int a, b, c, d;
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	float t1 = 0;
	float t2 = 1;
	a=SolveNonLinearEquation(-dx, P1.x - r.Left, t1, t2);
	b=SolveNonLinearEquation(dx, r.Right - P1.x, t1, t2);
	c = SolveNonLinearEquation(dy, r.Bottom -P1.y, t1, t2);
	d = SolveNonLinearEquation(-dy, P1.y -r.Top, t1, t2);
	if (a*b*c*d) {
		Q1.x = P1.x + t1*dx;
		Q1.y = P1.y + t1*dy;
		Q2.x = P1.x + t2*dx;
		Q2.y = P1.y + t2*dy;
		return 1;
	}
	return 0;
}
