#include "Bezier.h"
#include <iostream>
#include "FillColor.h"
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	SDL_Color Color;
	Color = { 125,100,255,255 };
	CircleFill(p1.x, p1.y, 3, ren, Color);
	CircleFill(p2.x, p2.y, 3, ren, Color);
	CircleFill(p3.x, p3.y, 3, ren, Color);
	float t, x, y;
	SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	for (t = 0; t <= 1; t += 0.001) {
		x = p1.x*(1 - t)*(1 - t) + 2 * p2.x*(1 - t)*t + p3.x*t*t;
		y = p1.y*(1 - t)*(1 - t) + 2 * p2.y*(1 - t)*t + p3.y*t*t;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	SDL_Color Color;
	Color = { 255,100,0,200 };
	CircleFill(p1.x, p1.y, 3, ren, Color);
	CircleFill(p2.x, p2.y, 3, ren, Color);
	CircleFill(p3.x, p3.y, 3, ren, Color);
	CircleFill(p4.x, p4.y, 3, ren, Color);
	float t, x, y;
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	for (t = 0; t <= 1; t += 0.001) {
		x = p1.x*(1 - t)*(1 - t)*(1 - t) + 3 * p2.x*(1 - t)*t*(1 - t) + 3 * p3.x*t*t*(1 - t) + p4.x*t*t*t;
		y = p1.y*(1 - t)*(1 - t)*(1 - t) + 3 * p2.y*(1 - t)*t*(1 - t) + 3 * p3.y*t*t*(1 - t) + p4.y*t*t*t;
		SDL_RenderDrawPoint(ren, int(x + 0.5), int(y + 0.5));
	}
}


