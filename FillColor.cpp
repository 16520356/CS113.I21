#include "FillColor.h"
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */

int findHighestBitSet(Uint32 num)
{
	int order = 0, i;

	/* Iterate over each bit of integer */
	for (i = 0; i<INT_SIZE; i++)
	{
		/* If current bit is set */
		if ((num >> i) & 1)
			order = i;
	}

	return order;
}

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	//Check if pixel is a 32-bit integer
	if (findHighestBitSet(pixel) == 31)
	{
		/* Get Alpha component */
		temp = pixel & fmt->Amask;  /* Isolate alpha component */
		temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
		temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
		alpha = (Uint8)temp;
	}
	else {
		alpha = 255;
	}

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;


	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
		return NULL;
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
		}
	}
	return saveSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}


bool canFilled(SDL_Window *win, Vector2D newPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Surface *surface = getPixels(win, ren);

	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	int w = surface->w;

	int index = newPoint.y * w + newPoint.x;
	Uint32 pixel = pixels[index];
	SDL_Color color = getPixelColor(pixel_format, pixel);
	//cout << (int)color.r << "," << (int)color.g << "," << (int)color.b << "," << (int)color.a << endl;

	if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
	{
		return true;
	}

	return false;
}

struct Diem {
	int x;
	int y;
};

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	vector<Diem> stack;
	Diem Start;
	Start.x = startPoint.x;
	Start.y = startPoint.y;
	stack.push_back(Start);
	Vector2D nextPoint;
	Diem x;
	while (stack.size()!=0)
	{
		int end = stack.size() - 1;
		SDL_RenderDrawPoint(ren, stack[end].x, stack[end].y);
		nextPoint.x = stack[end].x - 1;
		nextPoint.y = stack[end].y;
		cout << stack[end].x << " " << stack[end].y << endl;
		if (canFilled(win, nextPoint, pixel_format, ren, fillColor, boundaryColor) == true && nextPoint.x > 0 && nextPoint.y > 0 && nextPoint.x < 600 && nextPoint.y < 800) {
			x.x = nextPoint.x;
			x.y = nextPoint.y;
			stack.push_back(x);
			continue;
		}
		nextPoint.x = stack[end].x + 1;
		nextPoint.y = stack[end].y;
		if (canFilled(win, nextPoint, pixel_format, ren, fillColor, boundaryColor) == true && nextPoint.x > 0 && nextPoint.y > 0 && nextPoint.x < 600 && nextPoint.y < 800) {
			x.x = nextPoint.x;
			x.y = nextPoint.y;
			stack.push_back(x);
			continue;
		}
		nextPoint.x = stack[end].x;
		nextPoint.y = stack[end].y - 1;
		if (canFilled(win, nextPoint, pixel_format, ren, fillColor, boundaryColor) == true && nextPoint.x > 0 && nextPoint.y > 0 && nextPoint.x < 600 && nextPoint.y < 800) {
			x.x = nextPoint.x;
			x.y = nextPoint.y;
			stack.push_back(x);
			continue;
		}
		nextPoint.x = stack[end].x;
		nextPoint.y = stack[end].y + 1;
		if (canFilled(win, nextPoint, pixel_format, ren, fillColor, boundaryColor) == true && nextPoint.x > 0 && nextPoint.y > 0 && nextPoint.x < 600 && nextPoint.y < 800) {
			x.x = nextPoint.x;
			x.y = nextPoint.y;
			stack.push_back(x);
			continue;
		}
		stack.pop_back();
	}
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int max = a;
	if (max < b)
		max = b;
	if (max < c)
		max = c;
	return max;
}

int minIn3(int a, int b, int c)
{
	int min = a;
	if (min > b)
		min = b;
	if (min > c)
		min = c;
	return min;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D t;
	t = a;
	a = b;
	b = t;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	int max = maxIn3(v1.y, v2.y, v3.y);
	int min = minIn3(v1.y, v2.y, v3.y);
	if (v1.y == max&&v2.y == min) {
		swap(v1, v2);
		swap(v2, v3);
	}
	if (v1.y == min&&v2.y == max) {
		swap(v2, v3);
	}
	if (v1.y == max&&v3.y == min) {
		swap(v1, v3);
	}
	if (v1.y == min&&v3.y == max);
	if (v2.y == max&&v3.y == min) {
		swap(v1, v3);
		swap(v2, v1);
	}
	if (v2.y == min&&v3.y == max) {
		swap(v2, v1);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xmax = maxIn3(v1.x, v2.x, v3.x);
	int xmin = minIn3(v1.x, v2.x, v3.x);
	for (int i = xmin; i <= xmax; i++)
		SDL_RenderDrawPoint(ren, i, v1.y);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float dx1 = v3.x - v1.x;
	float dx2 = v3.x - v2.x;
	float dy1 = v3.y - v1.y;
	float dy2 = v3.y - v2.y;
	float dm1 = (float)(dx1 / dy1);
	float dm2 = (float)(dx2 / dy2);
	float xl = v3.x;
	float xr = v3.x;
	for (int i = v3.y; i >= v1.y; i--) {
		Bresenham_Line(int(xl + 0.5), i, int(xr + 0.5), i, ren);
		xl -= dm1;
		xr -= dm2;
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float dx1 = v3.x - v1.x;
	float dx2 = v2.x - v1.x;
	float dy1 = v3.y - v1.y;
	float dy2 = v2.y - v1.y;
	float dm1 = (float)(dx1 / dy1);
	float dm2 = (float)(dx2 / dy2);
	float xl = v1.x;
	float xr = v1.x;
	for (int i = v1.y; i <= v3.y; i++) {
		Bresenham_Line(int(xl + 0.5), i, int(xr + 0.5), i, ren);
		xl += dm1;
		xr += dm2;
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float dx1 = v3.x - v1.x;
	float dx2 = v2.x - v1.x;
	float dy1 = v3.y - v1.y;
	float dy2 = v2.y - v1.y;
	float dx3 = v3.x - v2.x;
	float dy3 = v3.y - v2.y;
	float dm1 = (float)(dx1 / dy1);
	float dm2 = (float)(dx2 / dy2);
	float dm3 = (float)(dx3 / dy3);
	float xl = v1.x;
	float xr = v1.x;
	for (int i = v1.y; i <= v2.y; i++) {
		Bresenham_Line(int(xl + 0.5), i, int(xr + 0.5), i, ren);
		xl += dm1;
		xr += dm2;
	}
	for (int i = v2.y; i <= v3.y; i++) {
		Bresenham_Line(int(xl + 0.5), i, int(xr + 0.5), i, ren);
		xl += dm1;
		xr += dm3;
	}
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y) {
		if (v2.y == v3.y)
			TriangleFill1(v1, v2, v3, ren, fillColor);
		else
			TriangleFill2(v1, v2, v3, ren, fillColor);
	}
	else {
		if (v2.y == v3.y)
			TriangleFill3(v1, v2, v3, ren, fillColor);
		else
			TriangleFill4(v1, v2, v3, ren, fillColor);
	}
}

int NhoNhat(int a, int b) {
	if (a < b)
		return a;
	return b;
}
int LonNhat(int a, int b) {
	if (a > b)
		return a;
	return b;
}
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((xc - x)*(xc - x) + (yc - y)*(yc - y) <= R*R)
		return true;
	return false;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymin = NhoNhat(y1, y2);
	int ymax = LonNhat(y1, y2);
	for (int y = ymin; y <= ymax; y++)
		if (isInsideCircle(xc, yc, R, x1, y) == true)
			SDL_RenderDrawPoint(ren, x1, y);
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xmin = NhoNhat(vTopLeft.x, vBottomRight.x);
	int xmax = LonNhat(vTopLeft.x, vBottomRight.x);
	for (int x = xmin; x <= xmax; x++)
		FillIntersection(x, vTopLeft.y, x, vBottomRight.y, xc, yc, R, ren, fillColor);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xmin = NhoNhat(vTopLeft.x, vBottomRight.x);
	int xmax = LonNhat(vTopLeft.x, vBottomRight.x);
	int ymin = NhoNhat(vTopLeft.y, vBottomRight.y);
	int ymax = LonNhat(vTopLeft.y, vBottomRight.y);
	for (int x = xmin; x <= xmax; x++)
		Bresenham_Line(x, ymin, x, ymax, ren);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	Bresenham_Line(xc + x, yc + y, xc + x, yc - y, ren);
	Bresenham_Line(xc - x, yc + y, xc - x, yc - y, ren);
	Bresenham_Line(xc + y, yc + x, xc + y, yc - x, ren);
	Bresenham_Line(xc - y, yc + x, xc- y, yc - x, ren);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = R;
	int y = 0;
	int p = 3 - 2 * R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x > y) {
		if (p <= 0)
			p += 4 * y + 6;
		else {
			p += 4 * y - 4 * x + 10;
			x--;
		}
		y++;
		put4line(xc, yc, x, y, ren, fillColor);
	}
}

void Put2IntersectionLine(int x, int y, int xce, int yce, int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersection(xce + x, yce + y, xce + x, yce - y, xc, yc, R, ren, fillColor);
	FillIntersection(xce - x, yce + y, xce - x, yce - y, xc, yc, R, ren, fillColor);
}
void FillIntersectionEllipseCircle(int xcE, int ycE, int RE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = 0, y = b;
	float a4 = a*a;
	a4 *= a;
	a4 *= a;
	float z = a4 / (a*a + b*b);
	int p = -2 * a*a*b + a*a + 2 * b*b;
	Put2IntersectionLine(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	while (x <= sqrt(z)) {
		if (p <= 0)
			p += 4 * b*b*x + 6 * b*b;
		else {
			p += 4 * b*b*x - 4 * a*a*y;
			y--;
		}
		x++;
		Put2IntersectionLine(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	}
	// Area 2
	x = a, y = 0;
	p = b*b + 2 * a*a - 2 * a*b*b;
	Put2IntersectionLine(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	while (x > sqrt(z)) {
		if (p <= 0)
			p += 4 * a*a*y + 6 * a*a;
		else {
			p += 4 * a*a*y - 4 * b*b*x;
			x--;
		}
		y++;
		Put2IntersectionLine(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	}
}

void Put4IntersectionLine(int x, int y, int xc1, int yc1, int xc2, int yc2, int R, SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersection(xc1 + x, yc1 + y, xc1 + x, yc1 - y, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 - x, yc1 + y, xc1 - x, yc1 - y, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 + y, yc1 + x, xc1 + y, yc1 - x, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 - y, yc1 + x, xc1 - y, yc1 - x, xc2, yc2, R, ren, fillColor);
}
void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	
	int x = R1;
	int y = 0;
	int p = 3 - 2 * R1;
	Put4IntersectionLine(x, y, xc1, yc1, xc2, yc2, R2, ren, fillColor);
	while (x > y) {
		if (p <= 0)
			p += 4 * y + 6;
		else {
			p += 4 * y - 4 * x + 10;
			x--;
		}
		y++;
		Put4IntersectionLine(x, y, xc1, yc1, xc2, yc2, R2, ren, fillColor);
	}
}
